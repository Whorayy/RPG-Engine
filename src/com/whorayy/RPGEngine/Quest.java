package com.whorayy.RPGEngine;

public class Quest {
	private String questTitle;
	private String questDesc;
	private Boolean isQuestDone;
	
	Quest(String title, String desc)
	{
		this.questTitle = title;
		this.questDesc = desc;
		this.isQuestDone = false;
	}
	
	public Boolean checkQuest()
	{
		return this.isQuestDone;
	}
	
	public void setQuestDone()
	{
		this.isQuestDone = true;
	}
	
	public String toString()
	{
		return this.questTitle + "\n" + this.questDesc + "\n";
	}
}
