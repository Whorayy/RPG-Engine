package com.whorayy.RPGEngine;

public class Player {
	private String name;
	
	Player()
	{
		this.name = EngineMain.dlg.readString("Enter your name: ");
	}
	
	public String getName()
	{
		return this.name;
	}
}
