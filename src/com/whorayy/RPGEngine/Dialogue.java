package com.whorayy.RPGEngine;

import java.util.Scanner;

public class Dialogue {
	private Scanner dlg_input;
	
	Dialogue()
	{
		dlg_input = new Scanner(System.in);
	}
	
	public String readString(String prompt)
	{
		if (prompt.length() > 0)
		{
			print(prompt);
			return this.dlg_input.nextLine();
		}
		
		else
		{
			return this.dlg_input.nextLine();
		}
	}
	
	public int readInt(String prompt)
	{
		if (prompt.length() > 0)
		{
			print(prompt);
			return this.dlg_input.nextInt();
		}
		
		else
		{
			return this.dlg_input.nextInt();
		}
	}
	
	public float readFloat(String prompt)
	{
		if (prompt.length() > 0)
		{
			print(prompt);
			return this.dlg_input.nextFloat();
		}
		
		else
		{
			return this.dlg_input.nextFloat();
		}
	}
	
	public double readDouble(String prompt)
	{
		if (prompt.length() > 0)
		{
			print(prompt);
			return this.dlg_input.nextDouble();
		}
		
		else
		{
			return this.dlg_input.nextDouble();
		}
	}
	
	public void print(String text)
	{
		System.out.print(text);
	}
}
