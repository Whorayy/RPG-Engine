package com.whorayy.RPGEngine;

public class Enemy {
	private String name;
	private int damage;
	private int health;
	
	Enemy(String name, int damage, int health)
	{
		this.name = name;
		this.damage = damage;
		this.health = health;
	}
	
	public int getDamage()
	{
		return this.damage;
	}
	
	public void getHurt(int amount)
	{
		health -= amount;
	}
}
